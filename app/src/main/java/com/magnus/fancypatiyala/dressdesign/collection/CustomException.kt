package com.magnus.fancypatiyala.dressdesign.collection

class CustomException(message: String) : Exception(message)
